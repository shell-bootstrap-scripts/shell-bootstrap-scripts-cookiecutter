
`set -o xtrace` exposes secret keys such as SSH keys. So does GitLab's `CI_DEBUG_TRACE`: https://gitlab.com/gitlab-org/gitlab/-/issues/21833

We want GitLab to print each of the lines in the script for debugging purposes --- when a script is going to be used very widely like this, if anything crashes we want to be able to quickly identify the offending line.

We cannot have the shell script as the canonical source, because the shell script (using `$(...)`) cannot be converted into YAML.
https://github.com/idank/bashlex#limitations

But the YAML can be converted into a shell script.

https://gitlab.com/gitlab-ci-yml-components/shell-bootstrap-scripts-cookiecutter.git
> for Docker images it's `sh` from alpine which is the base image for most of our tool images.

