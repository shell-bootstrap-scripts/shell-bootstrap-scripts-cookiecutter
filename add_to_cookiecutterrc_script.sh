printf "  CI_SERVER_HOST:" >> $HOME/.cookiecutterrc
echo " $CI_SERVER_HOST" >> $HOME/.cookiecutterrc
printf "  FIX_ALL_GOTCHAS_SCRIPT_LOCATION:" >> $HOME/.cookiecutterrc
echo " $CI_SERVER_URL/$NAMESPACE_COOKIECUT_TO/$repo_name/-/raw/$BRANCH_COOKIECUT_TO/fix_all_gotchas.sh" >> $HOME/.cookiecutterrc
if ! [ -z ${shell_bootstrap_scripts_directory_name+ABC} ]; then
  printf "  shell_bootstrap_scripts_directory_name:" >> $HOME/.cookiecutterrc
  echo " $shell_bootstrap_scripts_directory_name" >> $HOME/.cookiecutterrc
fi
if ! [ -z ${shell_bootstrap_scripts_namespace+ABC} ]; then
  printf "  shell_bootstrap_scripts_namespace:" >> $HOME/.cookiecutterrc
  echo " $shell_bootstrap_scripts_namespace" >> $HOME/.cookiecutterrc
fi
