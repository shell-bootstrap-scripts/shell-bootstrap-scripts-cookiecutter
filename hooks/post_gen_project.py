import pathlib
import shutil

import ruamel.yaml


def append_script_to_file(scriptYAML, filepath):
  with open(filepath, 'a') as omnibus:
    for index,line in enumerate(scriptYAML):
      print(line, file=omnibus)
      if index in mapLinesToCommentsAfterThatLine:
        entry = mapLinesToCommentsAfterThatLine[index]
        assert type(entry) is list
        assert len(entry) == 4
        # https://sourceforge.net/p/ruamel-yaml/code/ci/default/tree/tokens.py#l249
        commentToken = entry[0]
        assert type(commentToken) is ruamel.yaml.tokens.CommentToken
        comment = commentToken.value
        assert type(comment) is str
        omnibus.write(comment)
    # shutil.copyfileobj(open('set_up_networking.sh'), omnibus)
    # shutil.copyfileobj(open('set_up_jupyter.sh'), omnibus)


if __name__ == "__main__":
  # https://sourceforge.net/p/ruamel-yaml/code/ci/default/tree/main.py
  # You cannot use the safe loader and get comments.
  # Of course, in this case, anyone who has control of the YAML has control of the Python anyway.
  yaml = ruamel.yaml.YAML()
  scriptYAML = yaml.load(pathlib.Path('before_script.yaml'))
  assert type(scriptYAML) is ruamel.yaml.comments.CommentedMap
  before_script = scriptYAML['.job-that-requires-environment']['before_script']
  assert type(before_script) is ruamel.yaml.comments.CommentedSeq
  # https://sourceforge.net/p/ruamel-yaml/code/ci/default/tree/comments.py#l379
  # https://sourceforge.net/p/ruamel-yaml/code/ci/default/tree/comments.py#l184
  assert type(before_script.ca) is ruamel.yaml.comments.Comment
  # https://sourceforge.net/p/ruamel-yaml/code/ci/default/tree/comments.py#l41
  mapLinesToCommentsAfterThatLine = before_script.ca.items
  append_script_to_file(before_script, 'fix_all_gotchas.sh')
