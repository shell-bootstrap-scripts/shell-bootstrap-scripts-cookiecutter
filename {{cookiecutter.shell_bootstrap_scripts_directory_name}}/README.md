These scripts do not call each other. Rather, each file textually includes everything it needs.
This is so that you can always get everything you need with a single `wget --no-proxy {{ cookiecutter.FIX_ALL_GOTCHAS_SCRIPT_LOCATION }}` or `curl`.

Because of this, these scripts should not be edited directly.
These scripts are generated from the cookiecutter.

