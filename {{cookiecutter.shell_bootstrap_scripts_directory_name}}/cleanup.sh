# Some things cannot be done just with environment variables,
# some things require files,
# so we have to clean them up afterward.
# (Though really none of these files are a big deal.)
if ls environment.sh; then
  echo "rm environment.sh"
  rm environment.sh
fi
if ! [ -z ${http_proxy+ABC} ]; then
  echo "unset http_proxy"
  unset http_proxy
fi
if ! [ -z ${OS_PACKAGE_REPOSITORY_URLS+ABC} ]; then
  echo "unset OS_PACKAGE_REPOSITORY_URLS"
  unset OS_PACKAGE_REPOSITORY_URLS
fi
if ls fix_all_gotchas.sh; then
  echo "rm fix_all_gotchas.sh"
  rm fix_all_gotchas.sh
fi
if [ -z ${PROXY_CA_PEM+ABC} ]; then
  echo "PROXY_CA_PEM is unset, so assuming there is no .pem to delete."
else
  echo "rm $PROXY_CA_PEM"
  rm $PROXY_CA_PEM
  if ls bundled.pem; then
    echo "rm bundled.pem"
    rm bundled.pem
  fi
  if ls /etc/pki/ca-trust/source/anchors/$PROXY_CA_PEM; then
    echo "rm /etc/pki/ca-trust/source/anchors/$PROXY_CA_PEM"
    rm /etc/pki/ca-trust/source/anchors/$PROXY_CA_PEM
  fi
  if ls /etc/ssl/certs/ca-cert-tmp-proxy-ca.pem.pem; then
    # I have no idea what creates this, maybe update-ca-certificates?
    echo "rm /etc/ssl/certs/ca-cert-tmp-proxy-ca.pem.pem"
    rm /etc/ssl/certs/ca-cert-tmp-proxy-ca.pem.pem
  fi
  if ls /usr/local/share/ca-certificates/tmp-proxy-ca.pem.crt; then
    # I have no idea what creates this, maybe update-ca-certificates?
    echo "rm /usr/local/share/ca-certificates/tmp-proxy-ca.pem.crt"
    rm /usr/local/share/ca-certificates/tmp-proxy-ca.pem.crt
  fi
  if ls /tmp/etc-ssl-certs-ca-certificates-before-before_script.crt; then
    ls -l /etc/ssl/certs/ca-certificates.crt
    echo "mv /tmp/etc-ssl-certs-ca-certificates-before-before_script.crt /etc/ssl/certs/ca-certificates.crt"
    mv /tmp/etc-ssl-certs-ca-certificates-before-before_script.crt /etc/ssl/certs/ca-certificates.crt
  fi
  if ls $HOME/.config/pip/pip.conf; then
    echo "s/cert = $PROXY_CA_PEM//"
    sed -i "s/cert = $PROXY_CA_PEM//" $HOME/.config/pip/pip.conf
    cat $HOME/.config/pip/pip.conf
  fi
fi
if ls $HOME/.curlrc; then
  CURLRC_CACERT_LINE="cacert=${PWD%/}/bundled.pem"
  echo "s|${CURLRC_CACERT_LINE}||"
  sed -i "s|${CURLRC_CACERT_LINE}||" $HOME/.curlrc
  cat $HOME/.curlrc
fi
if ls pki/nssdb/; then
  echo "rm -r pki/nssdb/"
  rm -r pki/nssdb/ || echo "Failed to remove pki/nssdb/ , possibly because it was created by a different user so this user does not own it."
fi
if ls $HOME/.pki/nssdb/; then
  echo "rm -r $HOME/.pki/nssdb/"
  rm -r $HOME/.pki/nssdb/
fi
if ! [ -z ${SSH_PRIVATE_DEPLOY_KEY+ABC} ] && command -v ssh-agent && command -v ssh-add; then
  ssh-add -D  # delete SSH key
  ssh-agent -k  # kill ssh-agent
fi
if ! [ -z ${DNF_ADDED_REPOSITORY_FILES+ABC} ]; then
  for REPO in $DNF_ADDED_REPOSITORY_FILES; do
    echo "rm /etc/yum.repos.d/$REPO"
    rm /etc/yum.repos.d/$REPO
  done
fi
echo 'rm cleanup.sh'
rm cleanup.sh
