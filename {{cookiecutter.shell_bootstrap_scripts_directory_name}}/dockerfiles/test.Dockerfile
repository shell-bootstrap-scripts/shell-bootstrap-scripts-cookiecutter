ARG BASE_IMAGE=alpine:edge
# This Dockerfile has a default BASE_IMAGE, but you can override it with --build-arg.

# To be used in the FROM, naturally an ARG must come before the FROM.
# However, any *other* ARG above the FROM will be silently ignored.
# So it's very important that the ARGs are below the FROM.
# https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
FROM $BASE_IMAGE
ARG ETC_ENVIRONMENT_LOCATION

ARG OS_PACKAGE_REPOSITORY_URLS
ENV OS_PACKAGE_REPOSITORY_URLS=${OS_PACKAGE_REPOSITORY_URLS}

COPY fix_all_gotchas.sh .
# Depending on the base image used, we might lack wget/curl/etc to fetch environment.sh,
# but the Kaniko image must have successfully fetched it so we can just copy it.
ADD fix_all_gotchas.sh .
ADD cleanup.sh .

RUN . ./fix_all_gotchas.sh \
    && echo "OS_PACKAGE_REPOSITORY_URLS=${OS_PACKAGE_REPOSITORY_URLS}" \
    && for URL in $OS_PACKAGE_REPOSITORY_URLS; do echo $URL; done \
    && sed -i -e 's/https/http/' /etc/apk/repositories \
    # Variables do not last outside of a RUN.
    # It's a feature. You don't want extraneous variables hanging around your Docker image.
    && apk add openssh-client py3-pip
# Now, with SSH installed, the script will pick up the SSH_PRIVATE_DEPLOY_KEY.

# The script sets several environment variables.
# Environment variables do *not* persist across Docker RUN lines.
# See also https://vsupalov.com/set-dynamic-environment-variable-during-docker-image-build/
# This allows Docker images to be portable to other networks if necessary.
RUN set -o allexport \
    && if [ -z ${FTP_PROXY+ABC} ]; then echo "FTP_PROXY is unset, so not doing any shenanigans."; else SSH_PRIVATE_DEPLOY_KEY="$FTP_PROXY"; fi \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    # Unfortunately, the -e flag is not enabled on all platforms,
    # so we cannot guarantee that we will stop here if before_script.sh crashes.
    && wget http://www.google.com/index.html && echo "wget works" && rm index.html \
    && python -m venv envwithnameless \
    && . envwithnameless/bin/activate \
    && python -m pip install --no-cache-dir nameless \
    && (ssh-add -D || echo "ssh-add -D failed, hopefully because we never installed openssh-client in the first place.") \
    && . ./cleanup.sh
