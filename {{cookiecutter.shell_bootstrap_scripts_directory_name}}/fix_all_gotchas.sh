# This script will attempt to set up any container you might have.

# This script is not a panacea.
# It doesn't obviate the need to know what you need from a container.
# But the intent of this script is to allow you to get going immediately with any image you want to use.

right_after_pull_docker_image=$(date +%s)

# First we print a bunch of system information.
# Why? Because when picking up an image, you might not actually know the details of the operating system on it.
cat /etc/os-release || echo "cat /etc/os-release failed."
lsb_release -a || echo "lsb_release -a failed."
hostnamectl || echo "hostnamectl failed."
uname -r || echo "uname -r failed."
echo $(whoami)
echo $USER

